import axios from 'axios';
import Cookies from 'js-cookie';
import { testApiUrl } from '../environment';
import { BehaviorSubject } from 'rxjs';


export default class CoreService {
    public userToken: string | undefined;
    public basicDatas = new BehaviorSubject({});
    public userData = new BehaviorSubject({});

    constructor() {
        this.userToken = Cookies.get('XSRF-TOKEN')
        this.InitBasicDatas()
        this.InitUserDatas()
    }

    InitBasicDatas() {
        axios.get(`${testApiUrl}api/getinitdata`).then(
            (res) => {
                this.basicDatas.next(res.data)
            },
            (err) => console.warn(err)
        )
    }

    InitUserDatas() {
        axios.get(`${testApiUrl}getuserinfodata`).then(
            (res) => {
                console.log(res)
                this.userData.next(res.data)

            },
            (err) => {
                this.userData.next({})
                console.warn(err)}
        )
    }

    UserLogOut() {
        Cookies.remove('XSRF-TOKEN')
        Cookies.remove('laravel_session')
        window.location.replace('/signout')
    }
}

export const coreService = new CoreService();
