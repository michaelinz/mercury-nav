import axios from "axios";
import { coreService } from "./core.service";



export default function Intercetpor() {
    axios.interceptors.request.use(
        (req) => {
            // console.log(req)
            if (req.method != 'get'){
                
            }
            if (coreService.userToken ) {
                req.headers['X-XSRF-TOKEN'] = coreService.userToken
            }
            return req;
        },
        (err) => {
            return Promise.reject(err);
        }
    );

    axios.interceptors.response.use(
        (res) => {
            // Add configurations here

            return res;
        },
        (err) => {
            return Promise.reject(err);
        }
    );
}