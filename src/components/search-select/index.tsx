import React from 'react'
import { Dropdown } from 'semantic-ui-react'


export interface searchSelect {
    styles?: any, 
    placeholder?: string | 'Search ', 
    selectOptions: {}[],
    changes:(data:any)=>any
}

export default function SearchSelection({ styles, placeholder, selectOptions, changes }: searchSelect) {
    const handleChange = (e: any, { searchQuery, value }: { searchQuery: any, value: any }) => {
        // console.log(value)
        changes(value)
    }

    return (
        <Dropdown
            style={styles}
            placeholder={placeholder}
            fluid
            multiple
            search
            selection
            options={selectOptions}
            onChange={(e: any, { searchQuery, value }) => handleChange(e, { searchQuery, value })}
        />
    )

}

export function SearchSelectionSingle({ styles, placeholder, selectOptions, changes }: searchSelect) {
    const handleChange = (e: any, { searchQuery, value }: { searchQuery: any, value: any }) => {
        // console.log(value)
        changes(value)
    }

    return (
        <Dropdown
            style={styles}
            placeholder={placeholder}
            fluid
            search
            selection
            options={selectOptions}
            onChange={(e: any, { searchQuery, value }) => handleChange(e, { searchQuery, value })}
        />
    )
}