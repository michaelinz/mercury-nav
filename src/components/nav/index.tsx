import axios from 'axios';
import React, { useEffect, useRef, useState } from 'react';
import { testApiUrl } from '../../environment';
import { coreService } from '../../services/core.service';
import NavSearch from './nav-search';
import './styles.css';
declare var $: any;


export default function Nav() {
    const [displaySearch, setDisplaySearch] = useState(true);
    const [searchDropdown, setSearchDropdown] = useState(false)
    const [searchValue, setSearchValue] = useState('')
    const [userData, setUserData] = useState<{ [index: string]: any }>({})

    console.log(coreService.userToken)

    const searchArea = useRef(null);

    const scolled = () => {
        $(() => {
            $(window).on('scroll', () => {
                if ($(window).scrollTop() > 0) {
                    // $('.navbar').addClass('bg-light');
                    // $('.navbar').removeClass('bg-transparent');
                    $('#navSearchArea').addClass('d-none');
                    $('#navSearchArea').removeClass('d-flex');

                } else {
                    // $('.navbar').removeClass('bg-light');
                    // $('.navbar').addClass('bg-transparent');
                    $('#navSearchArea').removeClass('d-none');
                    $('#navSearchArea').addClass('d-flex');
                }
            });
        });
    }

    const handleClick = (e: any) => {
        const x: React.MutableRefObject<any> = searchArea;
        // console.log(x)
        // console.log(e.target)
        if (!x.current.contains(e.target)) {
            setSearchDropdown(false)
        }
    }

    useEffect(() => {
        coreService.userData.subscribe((res) => {
            console.log(res)
            if (res) {
                setUserData(res)
            }
        })


        // add when mounted
        document.addEventListener("mousedown", handleClick);
        // return function to be called when unmounted
        return () => {
            document.removeEventListener("mousedown", handleClick);
        };
    }, []);


    window.addEventListener('scroll', scolled);

    return (
        <div className="sticky-top">
            <nav style={{ backgroundColor: 'white' }} className="px-sm-5 px-3 pt-4 pb-4 navbar navbar-light navbar-expand-md">
                <a className="navbar-brand" href="/">
                    <img style={{ width: '160px' }} src="https://nxs1.s3-ap-southeast-2.amazonaws.com/images/logo.png" />
                </a>
                <button className="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                    <span className="navbar-toggler-icon"></span>
                </button>
                <div className="collapse navbar-collapse " id="navbarSupportedContent">
                    <div className="mx-auto">
                        <div ref={searchArea} onClick={() => setSearchDropdown(true)} id="navSearchArea" className={displaySearch ? "input-group d-flex mb-3 justify-content-center" : "input-group d-none mb-4 justify-content-center"} >
                            <div className="input-group-prepend position-relative">
                                <a style={{ zIndex: 9999 }} data-toggle="dropdown">
                                    <input onChange={(e) => setSearchValue(e.target.value)} onFocus={() => setSearchDropdown(true)} id="navSearchInput" placeholder={`Search here`} style={{}} className="navSearchInput" />
                                </a>
                                <div className={(searchDropdown ? 'd-block' : 'd-none') + " position-absolute p-0 pt-5 searchDropdown mx-auto dropdown-menu"}>
                                    <NavSearch searchValue={searchValue} />
                                </div>
                            </div>
                        </div>
                        <ul className="navbar-nav ">
                            <li className="nav-item ">
                                <a className="nav-link" href={'/'} >Home </a>
                            </li>
                            <li className="nav-item">
                                <a className="nav-link" href={'/calender'} >Graduate Calendar</a>
                            </li>
                            <li className="nav-item">
                                <a className="nav-link" href={'/employers'}>Employers</a>
                            </li>
                            <li className="nav-item">
                                <a className="nav-link" href={'/jobs'}>Jobs</a>
                            </li>
                            <li className="nav-item">
                                <a className="nav-link" href="/contact-us">Support</a>
                            </li>
                        </ul>
                    </div>
                    <ul className="navbar-nav ">
                        {(userData && userData.User) ? (<div><a onClick={() => coreService.UserLogOut()} className="nav-link" >Sign out</a> <br/>
                        <a className="nav-link" href="/student/favourite">Favourites
                        <i style={{ color: '#F18C76', fontSize: '20px' }} className="ml-2 fa fa-heart' : 'fa fa-heart-o"></i>
</a>
                        </div>):
                            <a href="#" className="nav-link" data-toggle="modal" data-target="#loginModal">Sign in</a>
                        }
                        <div></div>
                    </ul>
                </div>
            </nav>
        </div>
    )
}
