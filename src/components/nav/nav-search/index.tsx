import axios from 'axios';
import { isBefore } from 'date-fns';
import React, { useEffect, useState } from 'react';
import { testApiUrl, resourceUrl } from '../../../environment';
import { coreService } from '../../../services/core.service';
import SearchSelection from '../../search-select';
import './styles.css';
declare var $: any;


export default function NavSearch({searchValue}:{searchValue:string}) {
    const [displaySearch, setDisplaySearch] = useState(true);
    // const [resentSearches, setResentSearches] = useState();

    const [searchLocation, setSearchLocation] = useState([])
    const [searchSubject, setSearchSubject] = useState([])
    const [searchIndustry, setSearchIndustry] = useState([])
    const [searchType, setSearchType] = useState<string | null>(null)
    const [supplimentaryData, setSupplimentaryData] = useState<{[index:string]:any}  > ({})

    // const history = useHistory();

    const search = () => {
        const subjects = searchSubject.reduce((prevVal,currVal,index)=>{
            return index == 0 ? ('subjects=' + currVal ) : (prevVal + ',' + currVal );
        }, '&') 

        const industries = searchIndustry.reduce((prevVal,currVal,index)=>{
            return index == 0 ? ('industries=' + currVal ) : (prevVal + ',' + currVal );
        }, '&')

        const locations = searchLocation.reduce((prevVal,currVal,index)=>{
            return index == 0 ? ('locations=' + currVal ) : (prevVal + ',' + currVal );
        }, '&')

        const search = 'search=' + searchValue + '&'

        const url = (searchType=='employers'?'employers':'jobs') + '?' + subjects + industries + locations + search
        console.log(url)
        if (url){
            window.location.replace('/'+url) 
            // history.push('/'+url)
        }
    }

    useEffect(()=>{
        coreService.basicDatas.subscribe((res)=>{
            setSupplimentaryData(res)
        })
    }, [])

    return (
        <span>
            {supplimentaryData && supplimentaryData.searchResults &&
                <div style={{ color: '#7B7B7B' }}>
                    <div className="px-4">
                        <p>People have search for</p>
                        <div className="row">
                            {supplimentaryData.searchResults.map((data:any, index:number)=>{
                                return (<a href={'/employerProfile/'+data.relevantId} className="d-block px-2 col-2" key={index}>
                                    <img style={{width:'100%'}} src={resourceUrl + '/' + supplimentaryData.employers.find((employer:any)=>employer.id == data.relevantId)?.company_logo}/>
                                </a>)
                            })}
                        </div>
                        <p className="pt-3 pb-3">Refine your search...</p>
                    </div>
                    <div className="accordion " id={'ae1'}>
                        <div style={{ border: 'none', borderRadius: '0px', backgroundColor: "#F3F3F3", }} className="mb-1 p-3 px-4">
                            <div className="cardDropDownButton accordion-button collapsed d-flex justify-content-between" data-toggle="collapse" data-target={'#ael1'} aria-expanded="false" aria-controls={'collapse1'}>
                                <div>I am looking for</div>
                                <div>
                                    <i style={{ fontSize: '18px' }} className="text-dark fa fa-angle-right" aria-hidden="true"></i>
                                </div>
                            </div>
                            <div id={'ael1'} className="accordion-collapse collapse" aria-labelledby={'heading1'} data-parent={'#ae1'}>
                                <div className="pt-4 accordion-body d-flex">
                                    <div className="custom-control custom-radio custom-control-inline">
                                        <input onChange={(event) => setSearchType(event.target.value)} value="employers" type="radio" id="customRadioInline1" name="customRadioInline1" className="custom-control-input" />
                                        <label className="custom-control-label" htmlFor="customRadioInline1">Employers</label>
                                    </div>
                                    <div className="custom-control custom-radio custom-control-inline">
                                        <input onChange={(event) => setSearchType(event.target.value)} value="jobs" type="radio" id="customRadioInline2" name="customRadioInline1" className="custom-control-input" />
                                        <label className="custom-control-label" htmlFor="customRadioInline2">Jobs</label>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    {/* <div className="accordion " id={'ae2'}>
                        <div style={{ border: 'none', borderRadius: '0px', backgroundColor: "#F3F3F3", }} className="mb-1 p-3 px-4">
                            <div className="cardDropDownButton accordion-button collapsed d-flex justify-content-between" data-toggle="collapse" data-target={'#ael2'} aria-expanded="false" aria-controls={'collapse1'}>
                                <div>I am interested in a career in</div>
                                <div>
                                    <i style={{ fontSize: '18px' }} className="text-dark fa fa-angle-right" aria-hidden="true"></i>
                                </div>
                            </div>
                            <div id={'ael2'} className="accordion-collapse collapse" aria-labelledby={'heading1'} data-parent={'#ae2'}>
                                <div className="pt-4 accordion-body d-flex">
                                    <SearchSelection changes={(data) => { setSearchIndustry(data) }}
                                        styles={{ borderRadius: '10px', minHeight: '34px', width: '100%' }} placeholder='I am interested in a career in'
                                        selectOptions={supplimentaryData.industries.map((data: any) => ({ text: data.title, value: data.id, key: data.id }))} />
                                </div>
                            </div>
                        </div>
                    </div> */}
                    <div className="accordion " id={'ae3'}>
                        <div style={{ border: 'none', borderRadius: '0px', backgroundColor: "#F3F3F3", }} className="mb-1 p-3 px-4">
                            <div className="cardDropDownButton accordion-button collapsed d-flex justify-content-between" data-toggle="collapse" data-target={'#ael3'} aria-expanded="false" aria-controls={'collapse1'}>
                                <div>I am studying</div>
                                <div>
                                    <i style={{ fontSize: '18px' }} className="text-dark fa fa-angle-right" aria-hidden="true"></i>
                                </div>
                            </div>
                            <div id={'ael3'} className="accordion-collapse collapse" aria-labelledby={'heading1'} data-parent={'#ae3'}>
                                <div className="pt-4 accordion-body d-flex">
                                    <SearchSelection changes={(data) => { setSearchSubject(data) }}
                                        styles={{ borderRadius: '10px', minHeight: '34px', width: '100%' }} placeholder='I am studying'
                                        selectOptions={supplimentaryData.subjects.map((data: any) => ({ text: data.title, value: data.id, key: data.id }))} />
                                </div>
                            </div>
                        </div>
                    </div>
                    <div className="accordion " id={'ae4'}>
                        <div style={{ border: 'none', borderRadius: '0px', backgroundColor: "#F3F3F3", }} className="mb-1 p-3 px-4">
                            <div className="cardDropDownButton accordion-button collapsed d-flex justify-content-between" data-toggle="collapse" data-target={'#ael4'} aria-expanded="false" aria-controls={'collapse1'}>
                                <div>Where in NZ?</div>
                                <div>
                                    <i style={{ fontSize: '18px' }} className="text-dark fa fa-angle-right" aria-hidden="true"></i>
                                </div>
                            </div>
                            <div id={'ael4'} className="accordion-collapse collapse" aria-labelledby={'heading1'} data-parent={'#ae4'}>
                                <div className="pt-4 accordion-body d-flex">
                                    <SearchSelection changes={(data) => { setSearchLocation(data) }}
                                        styles={{ borderRadius: '10px', minHeight: '34px', width: '100%' }} placeholder='All of New Zealand'
                                        selectOptions={supplimentaryData.locations.map((data: any) => ({ text: data.title, value: data.id, key: data.id }))} />
                                </div>
                            </div>
                        </div>
                    </div>

                    <a onClick={() => search()} style={{ cursor: "pointer", borderRadius: '0px 0px 24px 24px' }} className="d-block bgc-orange text-center text-white p-3">
                        <b>Take the NxtStep</b>
                    </a>
                </div>
            }
        </span>
    )
}
